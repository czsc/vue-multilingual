# 功能描述
基于Vue的APP多语言处理，用例见 multiLanguage.html / multiLanguage-demo.html


# 依赖的模块

    libs/vue.js  

# 快速使用

首先创建一个多语言的数组，如下所示：（完整用法参见`multiLanguage.html`）

```
var langResource = {
    'zh': {
        'tip': '这是中文！'
    },
    'en': {
        'tip': 'This is English！'
    },
    'ja': {
        'tip': 'これは日本語です！'
    }
};
```

html内容如下

```
<div id="app">
    <div v-text="lang.tip"></div>
</div>
```

原理：通过将用户选择的语言类型保存到本地，每次加载选用相对应的语言对象数组渲染页面

```
var vuedata = {
    lang: {}
};
var vm = new Vue({
    el: '#app',
    data: vuedata
});

vuedata.lang = langResource['zh'];
```

demo文件中还提供了另一种方法，通过`loadScript`动态加载所需的语言文件包，用法参见`multiLanguage-demo.html`